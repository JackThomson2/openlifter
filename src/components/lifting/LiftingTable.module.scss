// vim: set ts=2 sts=2 sw=2 et:
//
// Theming for the main table on the Lifting page.
// The rest of the project uses Bootstrap tables, but since the Lifting page
// needs to be carefully designed, and doesn't need mobile support,
// it gets its own direct styling.

$row-height: 25px;
$cell-left-padding: 8px;
$row-font-size: 12px;

$color-dark: #232323;
$color-light: #ffffff;
$color-alternating-stripe: #f4f4f4;

$color-light-red-tint: #ffeeee;
$color-light-red-tint-stripe: #ffe3e3;
$color-light-green-tint: #eeffee;
$color-light-green-tint-stripe: #e3ffe3;

$border-rounding: 8px;


// Set the default colors.
table.liftingtable {
  color: $color-dark;
  background-color: $color-light;
  font-size: $row-font-size;
  box-shadow: 0 0 20px 0 hsla(0, 0%, 64%, .45);
}

.liftingtable th.smallCell {
  width: 52px;
}

// The header bar gets inverse colors.
// th is styled instead of thead to allow rounded corners.
.liftingtable th {
  color: $color-light;
  background-color: $color-dark;
  font-size: 11px;
  font-weight: bold;
}

// Cell alignment stuff.
.liftingtable td,
.liftingtable th {
  height: $row-height;
  padding: 0px $cell-left-padding 0px $cell-left-padding;
}
.liftingtable td.attemptInputCell {
  padding: 0px;
}

// Make the table striped.
.liftingtable tr:nth-child(even) {
  background-color: $color-alternating-stripe;
}
.liftingtable tr:nth-child(even) td.nolift {
  background-color: $color-light-red-tint-stripe;
}
.liftingtable tr:nth-child(even) td.goodlift {
  background-color: $color-light-green-tint-stripe;
}

// Rounded corners.
.liftingtable th:first-child {
  border-top-left-radius: $border-rounding;
}
.liftingtable th:last-child {
  border-top-right-radius: $border-rounding;
}
.liftingtable tr:last-child td:first-child {
  border-bottom-left-radius: $border-rounding;
}
.liftingtable tr:last-child td:last-child {
  border-bottom-right-radius: $border-rounding;
}

// Decoration for the current lifter's row.
.liftingtable tr.current {
  background-color: lightblue;
}

// A lifting cell marked "Good Lift".
.liftingtable td.goodlift {
  background-color: $color-light-green-tint;
  color: green;
}
// A lifting cell marked "No Lift".
.liftingtable td.nolift {
  background-color: $color-light-red-tint;
  color: red;
}

input.attemptInput {
  font-size: $row-font-size;
  border-radius: 0px;
  height: $row-height;
  padding-left: $cell-left-padding;
}
