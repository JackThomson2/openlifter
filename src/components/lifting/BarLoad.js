// vim: set ts=2 sts=2 sw=2 et:
// @flow
//
// This is the widget that gives a visual display of the weights on the bar,
// used by the loading crew.

import React from "react";
import { connect } from "react-redux";

import styles from "./BarLoad.module.scss";

type Props = {
  weightKg: number,
  rackInfo: string
};

type Plate = {
  weight: number,
  count: number,
  used: number,
  style: string
};

const Plate50: Plate = { weight: 50, count: 0, used: 0, style: styles.kg50 };
const Plate25: Plate = { weight: 25, count: 10, used: 0, style: styles.kg25 };
const Plate20: Plate = { weight: 20, count: 1, used: 0, style: styles.kg20 };
const Plate15: Plate = { weight: 15, count: 1, used: 0, style: styles.kg15 };
const Plate10: Plate = { weight: 10, count: 1, used: 0, style: styles.kg10 };
const Plate5: Plate = { weight: 5, count: 1, used: 0, style: styles.kg5 };
const Plate2p5: Plate = { weight: 2.5, count: 1, used: 0, style: styles.kg2p5 };
const Plate1p25: Plate = { weight: 1.25, count: 1, used: 0, style: styles.kg1p25 };
const Plate0p5: Plate = { weight: 0.5, count: 1, used: 0, style: styles.kg0p5 };
const Plate0p25: Plate = { weight: 0.25, count: 1, used: 0, style: styles.kg0p25 };

const PlateList = [Plate50, Plate25, Plate20, Plate15, Plate10, Plate5, Plate2p5, Plate1p25, Plate0p5, Plate0p25];

class Loading extends React.PureComponent<Props> {
  // Selects kilo plates using the simple greedy algorithm.
  selectKgPlates = () => {
    // The combined weight of the bar and collars, the lightest valid weight.
    const barAndCollarWeightKg = 25;
    const allPlates = PlateList.map(plate => ({ ...plate }));

    let sideWeightKg = (this.props.weightKg - barAndCollarWeightKg) / 2;
    let plates = [];

    for (let i = 0; i < allPlates.length; i++) {
      if (sideWeightKg === 0) return plates;

      let plate: Plate = allPlates[i];

      while (plate.count > 0 && sideWeightKg >= plate.weight) {
        plates.push(
          <div key={`${plate.weight}-${plate.count}`} className={plate.style}>
            <div>{plate.weight}</div>
            {plate.weight >= 25 && <div>{plate.used + 1}</div>}
          </div>
        );

        sideWeightKg -= plate.weight;
        plate.count--;
        plate.used++;
      }
    }

    if (sideWeightKg > 0) {
      plates.push(
        <div key={"error"} className={styles.error}>
          ?{sideWeightKg.toFixed(1)}
        </div>
      );
    }

    return plates;
  };

  render() {
    return (
      <div className={styles.container}>
        <div className={styles.bar} />
        {this.selectKgPlates()}
        <div className={styles.collar} />
        <div className={styles.bar} />

        <div className={styles.rackInfo}>Rack {this.props.rackInfo}</div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    ...state
  };
};

export default connect(
  mapStateToProps,
  null
)(Loading);
